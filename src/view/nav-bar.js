export default{
	data : function(){
		return {
			teams:[],
      news: [],
			id: 1,
			name:'',
			description:'',
			teamClicked: false,
			homeClicked: false,
      resultsClicked: false,
			errMessage:'',
      errMessageMatch: '',
			edit:false,
			idxEdit:null
		}
	},
	methods: {
		clickTeams: function() {
			if(!this.teamClicked){
				this.teamClicked=true;
				this.homeClicked=false;
				this.resultsClicked=false;
			}
			else{
				this.teamClicked=false;
			}
		},
		clickHome: async function() {
			if(!this.homeClicked){
        const api = new NewsService();
				let infos = await api.getNews();
        this.news = infos
        this.homeClicked=true;
        this.teamClicked=false;
        this.resultsClicked=false;
                
			}
			else{
				this.homeClicked=false;
			}
		},
    clickResults: function(){
      if(!this.resultsClicked)
      {
        this.resultsClicked = true;
        this.teamClicked=false;
        this.homeClicked=false;
      }
      else{
        this.resultsClicked = false
      }
    },
		checkFields: function() {
			this.errMessage="";
			document.querySelector("#name").removeAttribute("style");
			document.querySelector("#desc").removeAttribute("style");
			if(this.name===""||this.description===""){
				this.errMessage+="The";
				if(this.name===""){
					this.errMessage+=" name";
					document.querySelector("#name").setAttribute("style","border: 1px solid #d66");
				}
				if(this.description===""){
					this.errMessage+=" description";
					document.querySelector("#desc").setAttribute("style","border: 1px solid #d66");
				}
				this.errMessage+=" field must be required ";
			}
			else{
				if(this.name.lenght<5){
					this.errMessage+="The name must have at least 5 caracters ";
				}
				if(this.description.lenght<20){
					this.description+="The description must have at least 20 caracters ";
				}
			}
			if(this.errMessage.length===0){
				this.addTeam();
			}
		},
		addTeam: function(){
			if(!this.edit){//adding a team
				let team = new Team(this.id, this.name, this.description);
				console.log(team);
				this.teams.push(team);
				this.name='';
				this.description='';
				this.id=this.id+1
			}
			else{//editing a team
				let team = new Team(this.teams[this.idxEdit].id, this.name, this.description);
				console.log(team);
				this.teams[this.idxEdit]=team;
				this.name='';
				this.description='';
				this.edit=false;
			}
		},
		editTeam: function (team) {
			this.edit=!this.edit;
			this.teams.forEach((e,idx) => {
				if(e.id==team.id&&e.name==team.name&&e.description==team.description){
					this.idxEdit=idx;
				}
			});
			if(this.edit){
				this.name=team.name	;
				this.description=team.description;
			}
			else{
				this.name='';
				this.description='';
			}
		},
		Export:function(){
			console.log(JSON.stringify(this.teams))
		},
    Match: function(){
      this.errMessageMatch=""
      const equipe1 = document.querySelector("#equipe1").value
      const equipe2 = document.querySelector("#equipe2").value

      const stringEquipe1Score = document.querySelector("#equipe1Score").value
      const stringEquipe2Score = document.querySelector("#equipe2Score").value

      const equipe1Score = parseInt(stringEquipe1Score);
      const equipe2Score = parseInt(stringEquipe2Score);
      
      const res = this.teams.filter(item =>  item.name === equipe1)

      const res2 = this.teams.filter(item => item.name === equipe2)

      console.log(res)
      console.log(res2)

      let tab = [res,res2]

      if (equipe1Score=="" || equipe2Score=="") {
        this.errMessageMatch="Veuillez rentrer le score de TOUTES les equipes";
        return;
      }
      if (equipe1Score < 0 || equipe2Score < 0) {
        this.errMessageMatch = "Veuilez rentre un score positif";
        return
      }
      
      try {
        const api = ApiService();
        api.post("http://www.post-result.com",tab)
      } catch (Error) {
        this.errMessageMatch="Une erreur sur la requette POST"
      }
    }
	},
	template: 
		`
			<div>
				<span @click="clickHome" style="cursor: pointer">Home</span>
				<span @click="clickTeams" style="cursor: pointer">Teams</span>
				<span @click="clickResults" style="cursor: pointer">Results</span>
			</div>
      <div v-if="homeClicked">
      <news-card v-for="info in news" 
          :title="info.title"
          :date="info.publishedAt"/>
      </div>
			<div id="Teams" v-if="teamClicked">
				<form @submit.prevent>
				<br>
				<label>{{errMessage}}</label><br/>
			
				<div>
					<label>Name</label><br/>
					<input id="name" type="text" v-model="name"/>
				</div>

				<div>
					<label>Description</label><br/>
					<textarea id="desc" v-model="description"></textarea>
				</div>

				<input type="submit" value="Submit" v-on:click="checkFields"/>
				</form>
				<team-card @edit-team="editTeam" v-for="team in teams"
					:id="team.id"
					:nom="team.name"
					:description="team.description">
	 			</team-card>
				<input type="submit" value="Export" v-if="teams.length>0" v-on:click="Export" />
			</div>



      <div v-if="resultsClicked">
				<label>{{errMessageMatch}}</label><br/>
         <form @submit.prevent>
          <label for="equipe1">Choose a Team1:</label>
          <select name="equipe1" id="equipe1">
            <option v-for="team in teams"
              :value="team.name" 
              :label="team.name"/>
          </select>
          <span>Entre le score de l'équipe</span>
          <input type="number" id="equipe1Score">
          <br><br>
          <label for="equipe2">Choose a Team2:</label>
          <select name="equipe2" id="equipe2">
            <option v-for="team in teams"
              :value="team.name" 
              :label="team.name"/>
          </select>
          <span>Entre le score de l'équipe</span>
          <input type="number" id="equipe2Score">
          <br><br>
          <input type="submit" value="Submit" v-on:click="Match">
        </form> 
      </div>
		`
}
