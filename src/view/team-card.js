export default{
    emits: ['editTeam'],
    props: {
        id: Number,
        nom: String,
        description: String
    },
    methods: {
        edit: function() {
            this.$emit("editTeam", new Team(this.id,this.nom,this.description))
        }
	},
    template: `
        <br>
        <div id="teamCard">
            <span>{{id}}    {{nom}}    description: {{description}}</span>
            <span @click="edit" style="cursor: pointer"> - éditer</span>
        </div>
    `
}
