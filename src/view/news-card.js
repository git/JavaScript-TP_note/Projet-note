export default{
    props: {
        title: String,
        date: String
    },
    template: `
        <article>
            <span>{{title}}</span>
            <p>{{date.replace(/-/gi, "/").replace("Z", "").replace("T"," ").replace(":","h").split(":")[0]}}</p>
        </article>
    `
}
